# README #

Crud para agendar compromissos.

Para executar esta aplicação é necessário ter o back-end. Que pode ser acessado neste link: 

git clone https://lwazevedo@bitbucket.org/lwnProjetos/agenda-service.git

*Pós clone do back-end - siga as instruções;


### Pós clonar ! ###

* npm install
* bower install


### Executar a aplicação ! ###

* npm run serve

### Fazer o build ! ###

* npm run build

### O que vale destacar no código? ###

* Desenvolvido com angular components
* Gulp para as task
* Integração com NodeJS 
* Código simples
* ui-router

### O que poderia ser feito para melhorar o sistema ? ###

* Melhorar o uso dos components
* Melhorar o Layout da aplicação
* Melhorar a codificação


### Algo mais que você tenha a dizer ? ###

* Obrigado pela oportunidade