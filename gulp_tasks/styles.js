const gulp = require('gulp');
const browserSync = require('browser-sync');
const sourcemaps = require('gulp-sourcemaps');
const sass = require('gulp-sass');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
var bulkSass = require('gulp-sass-bulk-import');

const conf = require('../conf/gulp.conf');

gulp.task('styles', styles);

function styles() {
 return gulp.src(conf.path.src('index.scss'))
   .pipe(sourcemaps.init())
   .pipe(bulkSass())
   .pipe(sass())
   .pipe(postcss([autoprefixer()])).on('error', conf.errorHandler('Autoprefixer'))
   .pipe(sourcemaps.write())
   .pipe(gulp.dest(conf.path.tmp()))
   .pipe(browserSync.stream());
}