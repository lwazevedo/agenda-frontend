module.exports = {
  extends: [
    'angular'
  ],
  rules: {
    'angular/no-service-method': 0,
    'angular/log': 0
  },
    globals: {
    'moment': 0
  }
}
