function addController($http, Notification, $log, $state, $rootScope) {
  var self = this;
  self.teste = $rootScope.totalCount;
  self.comp = {};
  let _data = {};

  self.add = function () {
    if (self.formComp.$valid) {
      _data = self.comp;

      $http.post('http://localhost:1337/compromissos', _data)
      .then(function () {
        Notification.success({message: 'Compromisso salvo com sucesso.', positionY: 'top', positionX: 'center', delay: 2000});

        $state.go('list');
      }, function (data) {
        console.log(data.statusText, data.status);
        Notification.error({message: 'Ops. Ocorreu algum erro inesperado.', positionY: 'top', positionX: 'center', delay: 2000});
      });
    } else {
      Notification.error({message: 'Os campos com * ao lado devem ser preenchidos.', positionY: 'top', positionX: 'center', delay: 2000});
    }
  };
}

angular
.module('app')
.component('add', {
  templateUrl: 'app/components/add/add.html',
  controller: addController,
  transclude: true
});

