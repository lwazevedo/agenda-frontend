describe('add component', function () {
  beforeEach(module('app', function ($provide) {
    $provide.factory('add', function () {
      return {
        templateUrl: 'app/add.html'
      };
    });
  }));

  it('should...', angular.mock.inject(function ($rootScope, $compile) {
    var element = $compile('<add></add>')($rootScope);
    $rootScope.$digest();
    expect(element).not.toBeNull();
  }));
});
