function containerController() {
  this.text = 'My brand new component!';
}

angular
  .module('app')
  .component('container', {
    templateUrl: 'app/components/container/container.html',
    controller: containerController,
    transclude: true
  });

