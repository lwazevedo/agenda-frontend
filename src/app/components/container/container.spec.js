describe('container component', function () {
  beforeEach(module('app', function ($provide) {
    $provide.factory('container', function () {
      return {
        templateUrl: 'app/container.html'
      };
    });
  }));

  it('should...', angular.mock.inject(function ($rootScope, $compile) {
    var element = $compile('<container></container>')($rootScope);
    $rootScope.$digest();
    expect(element).not.toBeNull();
  }));
});
