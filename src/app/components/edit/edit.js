function editController($http, $state, $stateParams, Notification, $rootScope) {
  var self = this;
  self.teste = $rootScope.totalCount;
  self.id = $stateParams.id;
  let _data = {};

  init();

  function init() {
    getCompromisso();
  }

  function getCompromisso() {
    $http.get('http://localhost:1337/compromissos/' + self.id)
    .then(function (res) {
      self.compromisso = res.data;
    }, function (data) {
      console.log(data.statusText, data.status);
      Notification.error({message: 'Ops. Ocorreu algum erro inesperado.', positionY: 'top', positionX: 'center', delay: 2000});
    });
  }

  self.update = function () {
    if (self.formComp.$valid) {
      _data = self.compromisso;

      $http.post('http://localhost:1337/compromissos/' + self.id, _data)
      .then(function () {
        Notification.success({message: 'Compromisso atualizado com sucesso.', positionY: 'top', positionX: 'center', delay: 2000});
        $state.go('list');
      }, function (data) {
        console.log(data.statusText, data.status);
        Notification.error({message: 'Ops. Ocorreu algum erro inesperado.', positionY: 'top', positionX: 'center', delay: 2000});
      });
    } else {
      Notification.error({message: 'Os campos com * ao lado devem ser preenchidos.', positionY: 'top', positionX: 'center', delay: 2000});
    }
  };
}

angular
.module('app')
.component('edit', {
  templateUrl: 'app/components/edit/edit.html',
  controller: editController,
  transclude: true
});

