describe('edit component', function () {
  beforeEach(module('app', function ($provide) {
    $provide.factory('edit', function () {
      return {
        templateUrl: 'app/edit.html'
      };
    });
  }));

  it('should...', angular.mock.inject(function ($rootScope, $compile) {
    var element = $compile('<edit></edit>')($rootScope);
    $rootScope.$digest();
    expect(element).not.toBeNull();
  }));
});
