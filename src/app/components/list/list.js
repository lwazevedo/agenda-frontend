function listController($http, $log, Notification, $state, $rootScope) {
  var self = this;
  self.totalCompromissos = {};
  self.compromissos = {};
  self.totalPaginas = {};
  self.idDelete = {};
  self.page = 1;
  let _config = {};

  const _limit = 5;

  init();

  self.proximo = function () {
    if (self.page < self.totalPaginas + 1) {
      self.page += 1;
      getCompromissos();
    }
  };

  self.anterior = function () {
    if (self.page > 1) {
      self.page -= 1;
      getCompromissos();
    }
  };

  self.guardaId = function (id) {
    self.idDelete = id;
  };

  self.delete = function (id) {
    $http.delete('http://localhost:1337/compromissos/' + id)
    .then(function () {
      Notification.success({message: 'Compromisso deletado com sucesso.', positionY: 'top', positionX: 'center', delay: 2000});
      $state.reload();
    }, function (data) {
      console.log(data.statusText, data.status);
      Notification.error({message: 'Ops. Ocorreu algum erro inesperado.', positionY: 'top', positionX: 'center', delay: 2000});
    });
  };

  function init() {
    getCompromissos();
  }

  function getCompromissos() {
    _config.params = {};
    _config.params.page = self.page;
    _config.params.limit = _limit;

    $http.get('http://localhost:1337/compromissos', _config)
    .then(function (res) {
      self.compromissos = res.data.data;
      self.totalPaginas = Math.ceil(res.data.count / _limit);
      self.page = res.data.page;
      $rootScope.totalCount = res.data.count;
      self.totalCompromissos = res.data.count;
    }, function (data) {
      console.log(data.statusText, data.status);
      Notification.error({message: 'Ops. Ocorreu algum erro inesperado.', positionY: 'top', positionX: 'center', delay: 2000});
    });
  }
}

angular
.module('app')
.component('list', {
  templateUrl: 'app/components/list/list.html',
  controller: listController
});

