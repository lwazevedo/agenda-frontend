function viewController($http, $stateParams, $rootScope) {
  var self = this;
  self.teste = $rootScope.totalCount;

  self.compromisso = {};
  self.id = $stateParams.id;

  init();

  function init() {
    getCompromisso();
  }

  function getCompromisso() {
    $http.get('http://localhost:1337/compromissos/' + self.id)
    .then(function (res) {
      self.compromisso = res.data;
    }, function (data) {
      console.log(data.statusText, data.status);
      Notification.error({message: 'Ops. Ocorreu algum erro inesperado.', positionY: 'top', positionX: 'center', delay: 1000});
    });
  }
}

angular
.module('app')
.component('view', {
  templateUrl: 'app/components/view/view.html',
  controller: viewController
});

