describe('view component', function () {
  beforeEach(module('app', function ($provide) {
    $provide.factory('view', function () {
      return {
        templateUrl: 'app/view.html'
      };
    });
  }));

  it('should...', angular.mock.inject(function ($rootScope, $compile) {
    var element = $compile('<view></view>')($rootScope);
    $rootScope.$digest();
    expect(element).not.toBeNull();
  }));
});
