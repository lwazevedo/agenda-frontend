angular
  .module('app')
  .config(routesConfig)
  .run(function ($state) {
    $state.go('list');
  });

/** @ngInject */
function routesConfig($stateProvider, $urlRouterProvider, $locationProvider) {
  $locationProvider.html5Mode(true).hashPrefix('!');
  $urlRouterProvider.otherwise('/');

  $stateProvider
    .state('list', {
      url: '/',
      component: 'list'
    })
    .state('add', {
      url: '/add',
      component: 'add'
    })
    .state('view', {
      url: '/:id/view',
      component: 'view'
    })
    .state('edit', {
      url: '/:id/edit',
      component: 'edit'
    });
}
